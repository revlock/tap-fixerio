"""REST client handling, including FixerioStream base class."""

import requests
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta

from memoization import cached

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import APIKeyAuthenticator
from urllib import parse


SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class FixerioStream(RESTStream):
    """Fixerio stream class."""

    
    # OR use a dynamic url_base:
    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return self.config["api_url"]

    records_jsonpath = "$.[*]"  # Or override `parse_response`.
    init_date = None

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        today = datetime.utcnow().date()

        json_response = response.json()
        end_date = datetime.strptime(json_response["end_date"], "%Y-%m-%d").date()

        if end_date >= today:
            return None

        return end_date + timedelta(1)

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        today = datetime.utcnow().date()

        params: dict = {}
        if not self.init_date:
            self.init_date = self.get_starting_timestamp(context).date()

        params["access_key"] = self.config.get("access_key")
        params["base"] = self.config.get("base")
        start_date = (next_page_token or self.init_date)
        end_date = start_date + timedelta(365)

        if end_date >= today:
            end_date = today

        params["start_date"] = start_date.strftime("%Y-%m-%d")
        params["end_date"] = end_date.strftime("%Y-%m-%d")

        selected_properties = []
        for key, value in self.metadata.items():
            if isinstance(key, tuple) and len(key) == 2 and value.selected:
                field_name = key[-1]
                if field_name != 'date':
                    selected_properties.append(field_name)

        params['symbols'] = ",".join(selected_properties)

        return params

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        rates = response.json().get("rates")
        for date, rate in rates.items():
            rate["date"] = date
            yield rate
